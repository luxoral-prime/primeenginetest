require("@babel/polyfill");

const fs = require("fs");
const browserify = require("browserify");
const tsify = require("tsify");
const tinyify = require("tinyify");
const babelify = require("babelify");

const bundler = browserify({
    entries: ["source/Main.ts"],
    cache: {},
    packageCache: {}
})
    .plugin(tsify)
    //.plugin(tinyify)
    .transform(babelify, {
        presets: ["@babel/preset-env"],
        plugins: ["@babel/plugin-transform-runtime"],
        extensions: [".ts"]
    });

function bundle() {
    bundler.bundle(function (err, buf) {
        if (err) console.error(err);
        fs.writeFileSync("public/bundle.js", buf);
    });
}

bundle();
