import {
    Engine,
    Entity,
    Renderable,
    CanvasRenderingSystem,
    Scene,
    System,
    Rectangle,
    Transform,
    Polygon
} from "../engine/source/PrimeEngine";

export class RotatorSystem extends System {
    private angle: number = 0;

    public readonly queries = { Rotatable: [Transform] };

    public start(query: string, entities: Entity[]): void {}

    public update(query: string, entities: Entity[], delta: number): void {
        this.angle += delta;

        if (this.angle > Math.PI * 2) this.angle = 0;

        for (const entity of entities) {
            entity.components.get(Transform).rotation = parseInt(entity.name) % 2 === 0 ? this.angle : -this.angle;
        }

        // console.log("FPS: " + 1 / delta);
    }
}

function getRandomColor() {
    var letters = "0123456789ABCDEF";
    var color = "#";
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

async function main(): Promise<void> {
    const canvas = document.getElementById("PrimeEngine") as HTMLCanvasElement;
    
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    const engine = new Engine({
        canvas: canvas
    });

    const entities = [];

    for (let i = 0; i < 50; i++) {
        entities.push(
            new Entity(i.toString(), [Renderable, Transform, Rectangle])
        );

        entities[i].components.get(Renderable).fill = {
            enabled: true,
            style: getRandomColor()
        };

        entities[i].components.get(Renderable).stroke.enabled = true;
        entities[i].components.get(Renderable).stroke.style = getRandomColor();
        entities[i].components.get(Renderable).stroke.lineWidth = 20;
        entities[i].components.get(Renderable).offset = {
            x: -50,
            y: -50
        };

        entities[i].components.get(Transform).position = {
            x: Math.floor(Math.random() * canvas.width),
            y: Math.floor(Math.random() * canvas.height)
        };

        entities[i].components.get(Rectangle).height = 100;
        entities[i].components.get(Rectangle).width = 100;
    }

    for (let i = 50; i < 100; i++) {
        entities.push(
            new Entity(i.toString(), [Renderable, Transform, Polygon])
        );

        entities[i].components.get(Renderable).fill = {
            enabled: true,
            style: getRandomColor()
        };

        entities[i].components.get(Renderable).stroke.enabled = true;
        entities[i].components.get(Renderable).stroke.style = getRandomColor();
        entities[i].components.get(Renderable).stroke.lineWidth = 20;
        entities[i].components.get(Renderable).offset = {
            x: 0,
            y: 0
        };

        entities[i].components.get(Transform).position = {
            x: Math.floor(Math.random() * canvas.width),
            y: Math.floor(Math.random() * canvas.height)
        };

        const size = 50;

        entities[i].components.get(Polygon).vertices = [
            { x: size * Math.cos(0), y: size * Math.sin(0) },
            { x: size * Math.cos(2 * Math.PI / 6), y: size * Math.sin(2 * Math.PI / 6) },
            { x: size * Math.cos(2 * 2 * Math.PI / 6), y: size * Math.sin(2 * 2 * Math.PI / 6) },
            { x: size * Math.cos(3 * 2 * Math.PI / 6), y: size * Math.sin(3 * 2 * Math.PI / 6) },
            { x: size * Math.cos(4 * 2 * Math.PI / 6), y: size * Math.sin(4 * 2 * Math.PI / 6) },
            { x: size * Math.cos(5 * 2 * Math.PI / 6), y: size * Math.sin(5 * 2 * Math.PI / 6) },
        ];
    }

    const scene = new Scene(
        "Main",
        [...entities],
        [new CanvasRenderingSystem(engine), new RotatorSystem(engine)]
    );

    engine.scene = scene;
    engine.start();

    // window.addEventListener("resize", () => engine.setSize(Util.Fullscreen));
}

window.onload = main;
